import React from 'react';
import './App.css';
import logo from '../src/arrivu3.jpg';
import backgroundimage from '../src/background image.jpg';

import image from '../src/mobile.svg';
import profile from '../src/profilepic.svg';



function App() {
  return (
    
    <div className="App">
    <img className="wave" src={backgroundimage} />
    <div className="container">
      <div className="img">
      <img id="logo" src={logo} />  
         <img src={image} /> 
        
      </div> 
      <div className="login-container">
        <form>
          <img className="avatar" src={profile} />
          
          <h2>Welcome to Arrivu</h2>
          <div className="input-div one focus">
            <div className="i">
              <i class="fas fa-user"></i>
            </div>
            <div>
             <h5>LogIn:</h5>
             <input className="input" type="text" />
            </div>
          </div>
          <div className="input-div two">
            <div className="i">
              <i class="fas fa-lock"></i>
            </div>
            <div>
             <h5>Password:</h5>
             <input className="input" type="password" />
             </div>
             </div>
             <a href="#">Forgot Password</a>
             <input type="submit" className="btn" value="Login" />
        </form>
      </div>
    </div>
      
      
    
                   
    </div>
  );
}

export default App;
